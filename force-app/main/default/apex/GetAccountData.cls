public with sharing class GetAccountData {
    @AuraEnabled(cacheable=true)
    public static List<Contact> getAccountList() {
        return [SELECT Name FROM Contact];
    }
}
