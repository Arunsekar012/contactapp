import { LightningElement,track } from 'lwc';
import apexMethod from '@salesforce/apex/Namespace.Classname.apexMethod';

export default class ContactApp extends LightningElement {
    greeting = '';
  changeHandler(event) {
    this.greeting = event.target.value;
  }
    @track isShowModal = false;

    showModalBox() {  
        this.isShowModal = true;
    }

    hideModalBox() {  
        this.isShowModal = false;
    }
}