
import { LightningElement, wire } from 'lwc';
import getAccountList from '@salesforce/apex/GetAccountData.getAccountList';

 
export default class LWCWireEx extends LightningElement {
    @wire(getAccountList) Contact;
}